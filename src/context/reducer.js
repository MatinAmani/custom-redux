import { ADD_ITEM, REMOVE_ITEM, MARK_ITEM, CHANGE_THEME } from './actions'

export const initialState = {
    items: [],
    theme: 'dark',
}

export const reducer = (state, { type, payload }) => {
    switch (type) {
        case ADD_ITEM:
            return {
                ...state,
                items: [
                    ...state.items,
                    {
                        id: state.items.length,
                        title: payload.title,
                        marked: false,
                    },
                ],
            }

        case REMOVE_ITEM:
            return {
                ...state,
                items: state.items.filter((i) => i.id !== payload.id),
            }

        case MARK_ITEM:
            return {
                ...state,
                items: state.items.map((i) =>
                    i.id === payload.id ? { ...i, marked: !i.marked } : i
                ),
            }

        case CHANGE_THEME:
            return {
                ...state,
                theme: state.theme === 'dark' ? 'light' : 'dark',
            }

        default:
            return state
    }
}

export const selectItems = (state) => state.items
export const selectTheme = (state) => state.theme
