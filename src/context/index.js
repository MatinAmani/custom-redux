import { createContext, useContext, useReducer } from 'react'
import { reducer, initialState } from './reducer'

const Context = createContext()
const UpdateContext = createContext()

const useSelector = (getter) => getter(useContext(Context))
const useDispatch = () => useContext(UpdateContext)

const ContextProvider = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState)

    return (
        <Context.Provider value={state}>
            <UpdateContext.Provider value={dispatch}>
                {children}
            </UpdateContext.Provider>
        </Context.Provider>
    )
}

export { useSelector, useDispatch }
export default ContextProvider
