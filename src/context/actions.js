export const ADD_ITEM = 'ADD_ITEM'
export const addItem = (title) => ({
    type: ADD_ITEM,
    payload: {
        title,
    },
})

export const REMOVE_ITEM = 'REMOVE_ITEM'
export const removeItem = (id) => ({
    type: REMOVE_ITEM,
    payload: { id },
})

export const MARK_ITEM = 'MARK_ITEM'
export const markItem = (id) => ({
    type: MARK_ITEM,
    payload: { id },
})

export const CHANGE_THEME = 'CHANGE_THEME'
export const changeTheme = () => ({ type: CHANGE_THEME })
