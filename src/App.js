import React from 'react'
import ContextProvider, { useDispatch, useSelector } from './context'
import { selectTheme } from './context/reducer'
import { addItem, changeTheme } from './context/actions'
import { useInput } from './hooks'

import Items from './components/Items'

const App = () => {
    const theme = useSelector(selectTheme)

    const styles = {
        root: {
            backgroundColor: theme === 'dark' ? '#555' : '#fff',
        },
    }

    const dispatch = useDispatch()

    const { clearInput, ...input } = useInput('')

    const handleSubmit = (e) => {
        e.preventDefault()
        dispatch(addItem(input.value))
        clearInput()
    }

    return (
        <div className="rootContainer" style={styles.root}>
            <header>
                <button onClick={() => dispatch(changeTheme())}>
                    Change Theme
                </button>
                <form onSubmit={handleSubmit}>
                    <label htmlFor="input">Item Title:</label>
                    <input id="input" {...input} />
                    <button>Submit Item</button>
                </form>
            </header>
            <Items />
        </div>
    )
}

const ContextContainer = () => (
    <ContextProvider>
        <App />
    </ContextProvider>
)

export default ContextContainer
