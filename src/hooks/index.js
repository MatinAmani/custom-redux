import { useState } from 'react'

export const useInput = ({ initialValue, type = 'text' }) => {
    const [value, setValue] = useState(initialValue)

    const onChange = ({ target }) => setValue(target.value)

    const clearInput = () => setValue('')

    return { value, onChange, type, clearInput }
}
