import { useSelector } from '../context'
import { selectItems } from '../context/reducer'

const Item = ({ item }) => {
    return (
        <div className="item">
            <h3>{item.id}.</h3>
            <p>{item.title}</p>
        </div>
    )
}

const Items = () => {
    const items = useSelector(selectItems)
    return (
        <div className="items">
            {items.map((item) => (
                <Item key={item.id} item={item} />
            ))}
        </div>
    )
}

export default Items
